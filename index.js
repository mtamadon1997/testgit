const Redis = require("ioredis");
const redis = new Redis(6379, process.env.REDIS_HOST || '127.0.0.1'); // uses defaults unless given configuration object


const express = require('express')
const app = express()
const port = 3000

app.get('/', async (req, res) =>{
 res.send({
   cnt: await redis.incr("mycnt")
 })
})
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
